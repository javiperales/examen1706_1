@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">detalle de la lista{{$post->id}} </h1>
    <div class="card-body">

        <p><strong>titulo y comentario completo</strong></p>
            <h1>{{$post->title}}</h1>
             @if (isset(Session::get('posts')[$post->id]))
            <a href="/posts/{{$post->id}}/like" class="btn btn-warning"> me gusta</a>
             @else
              <p style="color:red">me gusta!!</p>
            <a href="/posts/{{$post->id}}/like" class="btn btn-warning"> decir que no me gusta</a>

            @endif
            <br>
            <br>
            @can('view', $post)
            <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">editar</a>
            @endcan
            <p>{{$post->content}}</p>
            <br>
            <p>{{$post->user->name}}: {{$post->date}}</p>

            <h2>comentarios</h2>
        @foreach($post->comments as $comment)

        <p>{{$comment->user->name}}:{{$comment->text}}</p>

        @endforeach

    </div>
<div class="card-footer">
     <form class="form-inline" method="post" action="/posts/{{$post->id}}/store">
        @csrf
  <div class="form-group">
    <label for="text">nuevo comentario:</label>
    <input type="text" class="form-control" id="text" name="text" placeholder="añadir texto">
  </div>
  <button type="submit" class="btn btn-default">añadir</button>
</form>
</div>
@endsection
