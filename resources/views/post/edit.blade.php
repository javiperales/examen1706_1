@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">{{$post->title}}</h1>
{{-- <a class="btn btn-primary" href="/lista/create">crear nueva lista</a> --}}


    <form class="form" method="post" action="/posts/{{$post->id}}">
         {{csrf_field()}}
         <input type="hidden" name="_method" value="PUT">

        {{-- <input type="text" name="content" value="{{old('content')}}"> --}}
        <textarea  rows="5" cols="50" name="content">
            {{$post->content}}
        </textarea>
    <br>

        <input class="btn btn-primary" type="submit"  value="guardar cambios">

    </form>
@endsection
