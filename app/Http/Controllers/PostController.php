<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use Session;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        $posts =Post::all();
        $start=0;
        $length=100;

        return view("post.index", ['posts'=>$posts ,'start'=>$start , 'length'=>$length]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'content'=>'required|max:500'
        ];

        $request->validate($rules);


        $post = Post::find($id);
        $post->fill($request->all());
        $post->user_id = \Auth::user()->id;
        $post->save();
        return redirect('/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

         /*$this->authorize('view', $post);*/
        return view('post.show', ['post'=>$post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
         $this->authorize('edit', $post);

        return view('post.edit',['post'=>$post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =[
            'content'=>'required|max:500'
        ];

        $request->validate($rules);


        $post = Post::find($id);
        $post->fill($request->all());
        $post->user_id = \Auth::user()->id;
        $post->save();
        return redirect('/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function storeComment($id, Request $request){

        $rules =[
            'text'=>'required|max:500'
        ];

        $request->validate($rules);


        $post = Post::find($id);
        $comment = new Comment;
        $comment->text = $request->text;
        $comment->post_id =$id;
        $comment->user_id=\Auth::user()->id;
        $comment->save();
        return back();


    }

    public function like($id ,Request $request )
    {
        $post = Post::findOrFail($id);
      $posts = Session::get('posts');

    if(isset($posts[$post->id])){
        unset($posts[$id]);
    }else{
        $posts[$id] =$post;
    }


    Session::put('posts' , $posts);
    return back();
    }
}
