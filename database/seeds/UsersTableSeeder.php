<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Pepe Navarro',
            'email' => 'pepe@gmail.com',
            'admin' => false,
            'password' => bcrypt('secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'Ana Sánchez',
            'email' => 'ana@gmail.com',
            'admin' => true,
            'password' => bcrypt('secret'),
        ]);
    }
}
